import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'app.dart';

const options = FirebaseOptions( 
  apiKey: "AIzaSyDq9hq2-ueAc1gO1l-5V_ENk02A2v7Nik0",
  authDomain: "chat-app-4b9e8.firebaseapp.com",
  projectId: "chat-app-4b9e8",
  storageBucket: "chat-app-4b9e8.appspot.com",
  messagingSenderId: "1009886652264",
  appId: "1:1009886652264:web:cd8cf25a7bc29cc93818dd"
);

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp(options: options);
  runApp(const App());
}
