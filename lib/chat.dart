// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class Chat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat"),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/signin');
            },
          )
        ],
      ),
      body: Column(children: [
        Expanded(
          child: ListView(
            reverse: true,
            children: [
              ListTile(
                leading: CircleAvatar(),
                title: Text("Oi, tudo bem?"),
                subtitle: Text("Fulano"),
                trailing: Text("21:32"),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder()
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.send),
                onPressed: () {},
              )
            ],
          ),
        ),
      ]),
    );
  }
}
