import 'package:flutter/material.dart';
import 'signin.dart';
import 'signup.dart';
import 'chat.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        "/signin": (context) => Signin(),
        "/signup": (context) => Signup(),
        "/chat": (context) => Chat(),
      },
      initialRoute: '/signin',
      theme: ThemeData(
        useMaterial3: false,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}